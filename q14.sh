#!/bin/bash
path=$(pwd)

echo '<html>' > $path/q14.html
echo '<head>' >> $path/q14.html
echo '<title>' >> $path/q14.html
echo TABLE OF DIRECTORIES AND FILES >> $path/q14.html
echo '</title>' >> $path/q14.html
echo '</head>' >> $path/q14.html
echo '<body background="https://www.babybedding.com/images/products/white-and-pink-polka-dot-cradle-sheet_medium.jpg">' >> $path/q14.html
echo '<table border="3">' >> $path/q14.html
n=0
for i in `find -type d`
do
	cd $i
	cpath=$(pwd)
	nelement=`ls -l|wc -l`
	if [ $nelement -gt 0 ]
	then
		rem=$((n%2))
		n=$((n+1))
		if [ $rem -eq 0 ]
		then
			bcolor=black
			fcolor=pink
		else
			bcolor=pink
			fcolor=black
		fi
		echo '<tr bgcolor="white">' >> $path/q14.html
		echo "<th colspan="3"> $cpath</th>" >> $path/q14.html
		echo '</tr>' >> $path/q14.html
		echo "<tr bgcolor="$bcolor">" >> $path/q14.html
		echo "<th><font color="$fcolor"> Name</font></th>" >> $path/q14.html
		echo "<th><font color="$fcolor"> Size</font></th>" >> $path/q14.html
		echo "<th><font color="$fcolor"> Type </font></th>" >> $path/q14.html
		echo "</tr>" >> $path/q14.html

		for j in `ls`
		do
			name=`echo $j`
			size=$(du -smh $j | awk '{print $1}')
			echo "<tr bgcolor="$bcolor">" >> $path/q14.html


			if [ -f $name ]
			then
				echo "<td><font color="$fcolor"> $name</font></td>" >> $path/q14.html
				echo "<td><font color="$fcolor"> $size</font></td>" >> $path/q14.html
				echo "<td><font color="$fcolor"> File </font></td>" >> $path/q14.html
			fi
			if [ -d $name ]
			then
				echo "<td><font color="$fcolor"> $name </font></td>" >> $path/q14.html
				echo "<td><font color="$fcolor"> $size </font></td>" >> $path/q14.html
				echo "<td><font color="$fcolor"> Dir </font></td>" >> $path/q14.html
			fi
			echo '</tr>' >> $path/q14.html
		done

	fi
	cd $path 
done
echo '</table>' >> $path/q14.html
echo '</body>' >> $path/q14.html
echo '</html>' >> $path/q14.html

firefox q14.html






