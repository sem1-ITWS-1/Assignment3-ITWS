#!/bin/bash

if [ $# -eq 3 ]
then
	m=$1
	d=`echo $2 | cut -d ',' -f1`
	y=$3

elif [ $# -eq 2 ]
then
	m=$1
	d=`echo $2 | cut -d ',' -f1`
	y=`echo $2 | cut -d ',' -f2`
else
	echo INVALID DATE
	exit
fi

echo "CHOOSE FORMAT 1)MM/DD/YYYY or 2)DD-MM-YYYY (type 1 or 2)"
read format



# if month is january
if [ "$m" == "jan" ] || [ "$m" == "Jan" ] || [ "$m" == "january" ] || [ "$m" == "January" ]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
	else
		mm=01
	fi
fi


# if month is february
if [ "$m" == "feb" ] || [ "$m" == "Feb" ] ||[ "$m" == "february" ] ||[ "$m" == "February" ]
then
	rem=$((y%4))
	if [ $rem -eq 0 ]
	then
		if [ $d -gt 29 ] || [ $d -lt 1 ]
		then
			echo INVALID DATE
			exit
		else
			mm=2
		fi
	else

		if [ $d -gt 28 ] || [ $d -lt 1 ]
		then
			echo INVALID DATE
			exit
		else
			mm=02 
		fi
	fi
fi


# if month is march
if [ "$m" == "mar" ] || [ "$m" == "Mar" ] ||[ "$m" == "march" ] ||[ "$m" == "March" ]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=03

	fi
fi


# if month is april
if [ "$m" == "apr" ] || [ "$m" == "april" ] ||[ "$m" == "Apr" ] ||[ "$m" == "April" ]
then
	if [ $d -gt 30 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=04

	fi
fi


# if month is may
if [ "$m" == "may" ] || [ "$m" == "May" ]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=05
	fi
fi


# if month is june
if [ "$m" == "jun" ] || [ "$m" == "Jun" ] ||[ "$m" == "june" ] ||[ "$m" == "June" ]
then
	if [ $d -gt 30 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=06
	fi
fi


# if month is july
if [ "$m" == "jul" ] || [ "$m" == "Jul" ] ||[ "$m" == "july" ] ||[ "$m" == "July" ]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=07

	fi
fi



# if month is august
if [ "$m" == "aug" ] || [ "$m" == "Aug" ] ||[ "$m" == "august" ] ||[ "$m" == "August" ]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=08
	fi
fi



# if month is september
if [ "$m" == "sep" ] || [ "$m" == "Sep" ] ||[ "$m" == "September" ] ||[ "$m" == "september" ]
then
	if [ $d -gt 30 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=09
	fi
fi



# if month is october
if [ "$m" == "oct" ] || [ "$m" == "Oct" ] ||[ "$m" == "October" ] ||[ "$m" == "october" ]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=10

	fi
fi


# if month is november
if [ "$m" == "nov" ] || [ "$m" == "Nov" ] ||[ "$m" == "november" ] ||[ "$m" == "November" ]
then
	if [ $d -gt 30 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=11

	fi
fi



# if month is december
if [ "$m" == "dec" ] || [ "$m" == "Dec" ] ||[ "$m" == "december" ] ||[ "$m" == "December" ]
then
	if [ $d -gt 31 ] || [ $d -lt 1 ]
	then
		echo INVALID DATE
		exit
	else
		mm=12

	fi
fi

if [ $format -eq 1 ]
then
	echo $mm/$d/$y
	exit
fi

if [ $format -eq 2 ]
then
	echo $d-$mm-$y
	exit
fi

echo INVALID OPTION

