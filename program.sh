#!/bin/bash

#  WRITE FUNCTION

if [ "$1" == "write" ]
then
	echo $2,$3,$4 >> employee.txt
	echo DONE
	exit
fi


#  READ FUNCTION

if [ "$1" == "read" ]
then
	# TO FIND BY ENO
	if [ "$2" == "eno" ]
	then
		count=0
		for i in `cat employee.txt`
		do
			eno=`echo $i | cut -d ',' -f1`
			if [ "$eno" == "$3" ]
			then
				echo $i
				count=$((count+1))
			fi
		done
		if [ $count -eq 0 ]
		then
			echo NO ENTRY FOUND
		fi
		exit
	fi

	# TO FIND BY ENAME
	if [ "$2" == "ename" ]
	then
		count=0
		for i in `cat employee.txt`
		do
			ename=`echo $i | cut -d ',' -f2`
			if [ "$ename" == "$3" ]
			then
				echo $i
				count=$((count+1))
			fi
		done
		if [ $count -eq 0 ]
		then
			echo NO ENTRY FOUND
		fi
		exit
	fi

	# TO FIND BY ESALARY
	if [ "$2" == "esalary" ]
	then
		count=0
		for i in `cat employee.txt`
		do
			esal=`echo $i | cut -d ',' -f3`
			if [ "$esal" == "$3" ]
			then
				echo $i
				count=$((count+1))
			fi
		done
		if [ $count -eq 0 ]
		then
			echo NO ENTRY FOUND
		fi
		exit
	else
		echo INVALID
	fi
fi


# UPDATE FUNCTION

if [ "$1" == "update" ]
then
	count=0
	for i in `cat employee.txt`
	do
		sed -i "/$2,/d" ./employee.txt
		echo $2,$3,$4 >> employee.txt
		count=$((count+1))
	done
	sed -i '/^\s*$/d' ./employee.txt 
	if [ $count -eq 0 ]
	then
		echo NO ENTRY FOUND 
		exit
	else
		echo DONE
		exit
	fi
fi

# DELETE FUNCTION
if [ "$1" == "delete" ]
then
	count=0
	for i in `cat employee.txt`
	do
		sed -i "/$2,/d" ./employee.txt
		count=$((count+1))
	done
	sed -i '/^\s*$/d' ./employee.txt
	if [ $count -eq 0 ]
	then
		echo NO ENTRY FOUND
		exit
	else
		echo DONE
		exit
	fi
fi

# DUPLICATE FUNCTION

if [ "$1" == "duplicate" ]
then
	prev=random
	count=0
	for i in `cat employee.txt | tr ',' ' ' | sort -n -k1,1 -k2,2 -k3,3 |tr ' ' ','`
	do
		if [ "$prev" != "$i" ]
		then
			prev=$i
			count=1
		else
			if [ $count -eq 1 ]
			then
				echo $i
				count=2
			else
				count=$((count+1))
			fi
		fi
	done
fi

#NTH SALARY FUNCTION
if [ "$1" == "nthsalary" ]
then
	sort -t ',' -n -k3 -r employee.txt > temp.txt
	prev=-1
	num=0
	for i in `cat temp.txt`
	do

		salary=`echo $i|cut -d ',' -f3`
		if [ $salary -ne $prev ]
		then
			num=$((num+1))
			prev=$salary
		fi
		if [ $num -eq $2 ]
		then
			echo $i
		fi
	done
fi









