#!/bin/bash
HISTFILE=~/.bash_history
set -o history
while read i
do
	if [ `echo $i|grep '|'|wc -l` -gt 0 ]
	then
		echo pipe >> store.txt
	else
		echo `echo $i|awk '{print $1}'` >> store.txt
	fi
done < ~/.bash_history

cat store.txt|sort|uniq -c|sort -n -r -k1,1|awk '{print $2"     "$1}'
rm store.txt

