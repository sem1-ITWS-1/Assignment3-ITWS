#!/bin/bash
declare -a arr
j=0
for i in `echo $1|fold -w1`
do
	arr[$j]=$i
	j=$((j+1))
done

len=0
flag=0
for i in ${arr[@]}
do
	len=$((len+1))
done

for (( i=0; i<$len; i++ ))
do
	j=$((len-i))
	j=$((j-1))
	if [ "${arr[$i]}" != "${arr[$j]}" ]
	then
		flag=1;
		break;
	fi
done
if [ $flag -eq 0 ]
then 
	echo Yes
else
	echo No
fi



