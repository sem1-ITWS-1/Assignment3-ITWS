#!/bin/bash
declare -a list
read list
for i in ${list[@]}
do
	x=`echo $i | tr '(' ' ' | tr ')' ' '`
	answer=`echo " $answer $x "`
done
answer=`echo $answer | tr -s ' '`
echo "($answer)"
