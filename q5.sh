#!/bin/bash
declare -a arr
j=0
for i in ${@:3}
do
	arr[$j]="-k$i,$i"
	j=$((j+1))
done
sort -n ${arr[@]} $1 $2
