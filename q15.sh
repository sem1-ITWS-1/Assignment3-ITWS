#!/bin/bash

wget --no-proxy -O $1.html -o message.txt $1
wget --no-proxy -O $2.html -o message.txt $2
#  paste -d"\n" $1.html $2.html 
cat $1.html $2.html|sed "s/[<][^>]*[>]/ /g"|tr '[:punct:]' ' '|tr ' ' '\n'|sort|sed '/^\s*$/d'|uniq -c|sort -n -r -k1,1|awk '{print $2"    "$1}'  > $3


# cat $1.html|fold -s|tr -d ' \t\r\f'|sort|grep -v '^$'|uniq -c|sort  -r -k1,1| awk '{print $2"     "$1}'
rm message.txt
