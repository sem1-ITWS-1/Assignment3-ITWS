#!/bin/bash
flag=0
sinput=`echo $1|fold -w1|sort|tr -d '\n'`
s1=`echo $sinput|cut -c 1`

for i in `compgen -c`
do
	
	com=`echo $i|fold -w1|sort|tr -d '\n'`
	c1=`echo $com|cut -c 1`
	if [ "$s1" == "$c1" ]
	then
		echo $com >> file.txt
		echo $i >> commands.txt
	fi
done


index=0
for i in `cat file.txt`
do
	index=$((index+1))
	if [ "$i" == "$sinput" ]
	then
		flag=1
		sindex=$index
		break
	fi
done

if [ $flag -eq 0 ]
then
	echo NOT FOUND
else
	comm=`cat commands.txt |sed "${index}q;d"`
	echo $comm
	$comm --help

fi
rm file.txt
rm commands.txt



