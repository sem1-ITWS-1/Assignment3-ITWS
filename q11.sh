#!/bin/bash
read op

# if operation is addition
if [ "$op" == "+" ]
then
	answer=0
	read n
	for (( i=0; i<n; i++ ))
	do
		read x
		answer=`echo $answer $x|awk '{print $1+$2}'`
	done
	printf "%.4g" $answer
	printf "\n"
	exit
fi

# if operation is subtraction
if [ "$op" == "-" ]
then
	read n
	read x
	answer=$x
	for (( i=1; i<n; i++ ))
	do
		read x
		answer=`echo $answer $x|awk '{print $1-$2}'`
	done
	printf "%.4g" $answer
	printf "\n"
	exit
fi

# if operation is multiplication
if [ "$op" == "*" ]
then
	answer=1
	read n
	for (( i=0; i<n; i++ ))
	do
		read x
		answer=`echo $answer $x|awk '{print $1*$2}'`
	done
	printf "%.4g" $answer
	printf "\n"
	exit
fi

# if operation is division
if [ "$op" == "/" ]
then
	read n
	read x
	answer=$x
	for (( i=1; i<n; i++ ))
	do
		read x
		answer=`echo $answer $x|awk '{print $1/$2}'`
	done
	printf "%.4g" $answer
	printf "\n"
	exit
fi
echo Please enter a valid operation

